﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionsController : UnitController
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void MovementAction()
    {
        if (actionsLeft > 0)
        {
            Debug.Log("calling movement action");
            actionsLeft--;
            DisplayCharacterValues.instance.SetActionsLeft(actionsLeft);
            //movementPointsLeft += this.movementSpeed;
            //Add movement points according to character's movement speed
            this.GetComponent<PlayerMove>().currentMovementPoints += this.movementSpeed;
            DisplayCharacterValues.instance.SetMovementPointsLeft(this.GetComponent<PlayerMove>().currentMovementPoints);
            //update the selectable tiles with every movement action
            this.GetComponent<PlayerMove>().FindSelectableTiles();
        }
    }

    public void AttackAction()
    {
        if (actionsLeft > 0)
        {
            actionsLeft--;
            DisplayCharacterValues.instance.SetActionsLeft(actionsLeft);
            //TODO: Add an attack
        }
    }
}
