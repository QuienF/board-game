﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnManager : MonoBehaviour
{
    public static Dictionary<string, List<TacticsMove>> units = new Dictionary<string, List<TacticsMove>>();
    public static Queue<string> turnKey = new Queue<string>();
    public static Queue<TacticsMove> turnTeam = new Queue<TacticsMove>();
    //the player who's turn it currently is
    public static GameObject currentPlayer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (turnTeam.Count == 0)
        {
            InitTeamTurnQueue();
        }


    }

    static void InitTeamTurnQueue()
    {
        List<TacticsMove> teamList = units[turnKey.Peek()];

        foreach (TacticsMove unit in teamList)
        {
            turnTeam.Enqueue(unit);
        }

        StartTurn();
    }
    public static void StartTurn()
    {
        //TODO: Add at start of turn effects
        //Should never be 0 so catch that case
        if (turnTeam.Count > 0)
        {
            //start the turn of the current team
            turnTeam.Peek().BeginTurn();
            Debug.Log(currentPlayer);
        }
    }
    public void EndTurn()
    {
        //take unit out of the turn queue and end its turn
        TacticsMove unit = turnTeam.Dequeue();
        unit.EndTurn();
        //is there any unit of the team left that has a turn, if so start its turn
        if (turnTeam.Count > 0)
        {
            StartTurn();
        }
        //the next team's turn
        else
        {
            //take the team out of the queue and enqueue it again so it is at the end of the queue again
            string team = turnKey.Dequeue();
            turnKey.Enqueue(team);
            InitTeamTurnQueue();
        }
    }
    public static void AddUnit (TacticsMove unit)
    {
        List<TacticsMove> list;
        //if the list does not contain the unit's team yet add it to the list
        if (!units.ContainsKey(unit.tag))
        {
            //added a new list to the dictionary for the tag
            list = new List<TacticsMove>();
            //unit's team
            units[unit.tag] = list;
            if (!turnKey.Contains(unit.tag))
            {
                //added unit's team to the turn queue
                turnKey.Enqueue(unit.tag);
            }
        }
        else
        {
            //grab the list for the tag and assign it to the unit
            list = units[unit.tag];
        }
        //add unit to the (dictionary) list
        list.Add(unit);
    }

    //TODO: Remove unit function, that removes the unit from the list when it is defeated or otherwise removed from the list (stun perhaps?)
}
