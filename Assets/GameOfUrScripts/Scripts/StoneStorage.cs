﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneStorage : MonoBehaviour
{/*
    public GameObject stonePrefab;
    public Tile startingTile;

    // Start is called before the first frame update
    void Start()
    {
        //Create one stone for each placeholder spot
        for (int i = 0; i < this.transform.childCount; i++)
        {
            GameObject theStone = Instantiate(stonePrefab);
            theStone.GetComponent<PlayerStone>().startingTile = this.startingTile;
            theStone.GetComponent<PlayerStone>().myStoneStorage = this;
            AddStoneToStorage(theStone, this.transform.GetChild(i));   
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AddStoneToStorage(GameObject theStone)
    {
        AddStoneToStorage(theStone, null);
    }

    void AddStoneToStorage(GameObject theStone, Transform thePlaceholder)
    {
        if (thePlaceholder == null)
        {
            //Find the first empty spaceholder
            for (int i = 0; i < this.transform.childCount; i++)
            {
                Transform p = this.transform.GetChild(i);
                if (p.childCount == 0)
                {
                    //This Placeholder is empty
                    thePlaceholder = p;
                    break;
                }
            }
            if (thePlaceholder == null)
            {
                Debug.LogError("Trying to add a stone but there is no empty space");
                return;
            }
        }
        //Parent the stone to the placeholder
        theStone.transform.SetParent(thePlaceholder);
        //Reset the stone's local position to 0,0,0 within the placeholder
        theStone.transform.localPosition = Vector3.zero;
    }*/
}
