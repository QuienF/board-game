﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : TacticsMove
{
    // Start is called before the first frame update
    void Start()
    {
        //Init() of TacticsMove
        Init();
        //&&if movement > 0
        if (turn == true)
        {
            //FindSelectableTiles();
            //this.GetComponent<LineOfSight>().GetLineOfSight();
        }
    }

    void FixedUpdate()
    {
        Debug.DrawRay(transform.position, transform.forward);
        //TODO: actions system, so you only get you movement when you choose move action
        //Don't execute anything if it is not the unit's turn
        if (turn == false)
        {
            return;
        }

        if (moving == false)
        {
            CheckMouse();
            if (selectableTilesFound==false) {
                FindSelectableTiles();
            }
        }
        else
        {
            //Move to selectable tile that was clicked on
            Move();
        }
    }
    void CheckMouse()
    {
        if (Input.GetMouseButtonUp(0))
        {
            //point a ray to where the mouse was clicked
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;
            if(Physics.Raycast(ray, out hit)==true)
            {
                if (hit.collider.tag == "Tile")
                {
                    Tile t = hit.collider.GetComponent<Tile>();
                    //if a selectable tile has been clicked set the target flag and move to it 
                    if (t.selectable == true)
                    {
                        //move to target tile
                        MoveToTile(t);
                    }
                }
                //TODO: show tooltips if clicking on a unit or icon
                //TODO: show tooltips on rightclick and / or when hovering the mouse over a unit or icon
            }

        }
    }
}
