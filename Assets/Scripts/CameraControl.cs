﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public float minCameraDistance;
    public float maxCameraDistance;
    //maximum distance for the camera to pan left or right
    public float maxCameraLeft;
    public float maxCameraRight;
    //Standard Camera Positions for the Players
    //Using standard camera positions when turn changes
    public Vector3 standardCameraPositionPlayer1;
    public Quaternion standardCameraRotationPlayer1;
    public Vector3 standardCameraPositionPlayer2;
    public Quaternion standardCameraRotationPlayer2;

    Vector3 desiredDirectionUpDown;
    bool upDownDirectionFound = false;
    Vector3 desiredDirectionLeftRight;
    bool leftRightDirectionFound = false;

    public float cameraSmoothTime;
    public float rotationSpeed;
    Vector3 velocity;


    float zoomAmount = 0; //With Positive and negative values
    float MaxToClamp = 10f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Rotate camera left when mousewheel is pressed and mouse is moved to the left
        //TODO: limit the rotation
        if (Input.GetMouseButton(2) && Input.GetAxis("Mouse X") < 0)
        {
            transform.Rotate(Vector3.up * rotationSpeed, Space.World);
        }
        //Rotate camera right when mousewheel is pressed and mouse is moved to the right
        else if (Input.GetMouseButton(2) && Input.GetAxis("Mouse X") > 0)
        {
            transform.Rotate(Vector3.down * rotationSpeed, Space.World);
        }

        float xAxisValue = Input.GetAxis("Horizontal");
        //zAxisValue is used to check in which direction the mouse wheel is being scrolled
        float zAxisValue = Input.GetAxis("Mouse ScrollWheel");
        float ws = Input.GetAxis("Vertical");
        zoomAmount += zAxisValue;
        //Zoom in using mousewheel as long as camera Y is further away as the min distance or zoom out as long as camera Y is not as far away as the max distance
        if ((gameObject.transform.position.y > minCameraDistance && zAxisValue > 0f) || (gameObject.transform.position.y < maxCameraDistance && zAxisValue < 0f))
        {
            zoomAmount = Mathf.Clamp(zoomAmount, -MaxToClamp, MaxToClamp);
            var translate = Mathf.Min(Mathf.Abs(Input.GetAxis("Mouse ScrollWheel")), MaxToClamp - Mathf.Abs(zoomAmount));
            gameObject.transform.Translate(0, 0, translate * rotationSpeed * Mathf.Sign(Input.GetAxis("Mouse ScrollWheel")));
        }
        //only get the direction for up and down movement of camera once to ignore camera rotation
        else if (Camera.current != null && upDownDirectionFound == false)
        {
            desiredDirectionUpDown = Vector3.Normalize(new Vector3(Camera.current.transform.position.x, transform.up.y, transform.up.z));
            upDownDirectionFound = true;
        }

        //Move camera up
        else if (Camera.current != null)
        {
            if (ws > 0)
            {
                //Camera.current.transform.Translate(new Vector3(0.0f, 0.0f, ws),Space.World);
                //Camera.current.transform.Translate(Vector3.up * ws);
                Camera.current.transform.Translate(desiredDirectionUpDown * ws);
            }
            //Move camera down
            else if (ws < 0)
            {
                //Camera.current.transform.Translate(new Vector3(0.0f, 0.0f, ws), Space.World);
                //Camera.current.transform.Translate(Vector3.up*ws);
                Camera.current.transform.Translate(desiredDirectionUpDown * ws);
            }

            if (leftRightDirectionFound == false)
            {
                desiredDirectionLeftRight = Vector3.Normalize(new Vector3(Camera.current.transform.right.x, Camera.current.transform.right.y, Camera.current.transform.right.z));
                leftRightDirectionFound = true;
            }

            //Move Camera left as long as camera X is not too far to the left
            if (xAxisValue < 0)
            {
                //Camera.current.transform.Translate(new Vector3(xAxisValue, 0.0f, 0.0f), Space.World);
                Camera.current.transform.Translate(desiredDirectionLeftRight * xAxisValue);
            }
            //Move Camera right as long as camera X is not too far to the right
            else if (xAxisValue > 0)
            {
                //Camera.current.transform.Translate(new Vector3(xAxisValue, 0.0f, 0.0f), Space.World);
                Camera.current.transform.Translate(desiredDirectionLeftRight * xAxisValue);
            }
        }
    }
    public void switchCameraPositionToNextPlayer(int playerId)
    {
            if (playerId == 0)
            {
                Camera.current.transform.position = standardCameraPositionPlayer1;
                //Smoothing doesnt work yet
                //Camera.current.transform.position = Vector3.SmoothDamp(Camera.current.transform.position, standardCameraPositionPlayer1, ref velocity, cameraSmoothTime);
                Camera.current.transform.rotation = Quaternion.Euler(standardCameraRotationPlayer1.x, standardCameraRotationPlayer1.y, standardCameraRotationPlayer1.z);
            }
            else if (playerId == 1)
            {
                Camera.current.transform.position = standardCameraPositionPlayer2;
                //smoothing doesnt work yet
                //Camera.current.transform.position = Vector3.SmoothDamp(Camera.current.transform.position, standardCameraPositionPlayer2, ref velocity, cameraSmoothTime);
                Camera.current.transform.rotation = Quaternion.Euler(standardCameraRotationPlayer2.x, standardCameraRotationPlayer2.y, standardCameraRotationPlayer2.z);
            }
            else
            {
                Debug.LogError("Player Id" + playerId + "for switchCameraPositionToNextPlayer does not exist");
                return;
            }
    }
}
