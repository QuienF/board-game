﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateManager : MonoBehaviour
{/*
    public int numberOfPlayers = 2;
    public int currentPlayerId;
    public int currentPlayerScore = 0;
    public int diceTotal;
    public bool isDoneRolling = false;
    public bool isDoneClicking = false;
    public bool automaticCameraswitch = false;
    //public bool isDoneAnimating = false;
    public int animationsPlaying = 0;

    public CameraControl cameraControl;

    public GameObject NoLegalMovesPopup;
    
    void Start()
    {
        currentPlayerScore = 0;
        for (int i = 0; i < numberOfPlayers; i++)
        {
            PlayerPrefs.SetInt("Player" + (i+1) + " Score",0);
            Debug.Log("Player" + (i+1) + " Score is" + PlayerPrefs.GetInt("Player" + (i+1) + " Score"));
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isDoneRolling==true && isDoneClicking==true && animationsPlaying==0)
        {
            //Turn is over
            NewTurn();
            return;
        }
    }


    public void NewTurn()
    {
       //save score of current player
       PlayerPrefs.SetInt("Player" + currentPlayerId + 1 + " Score", currentPlayerScore);

       isDoneRolling = false;
       isDoneClicking = false;
       //isDoneAnimating = false;

       currentPlayerId = (currentPlayerId+1) % numberOfPlayers;
        //set camera to standard position of according player
        if (automaticCameraswitch == true)
        {
            cameraControl.switchCameraPositionToNextPlayer(currentPlayerId);
        }
       //load score of following player
       currentPlayerScore = PlayerPrefs.GetInt("Player" + currentPlayerId + 1 + " Score");
       Debug.Log("Player Score: "+ PlayerPrefs.GetInt("Player" + currentPlayerId + 1 + " Score"));
    }

    public void RollAgain()
    {
        Debug.Log("RollAgain");
        isDoneRolling = false;
        isDoneClicking = false;
        //IsDoneAnimating = false;
    }

    public void CheckLegalTurns()
    {
        //if we rolled a zero we have no legal moves
        if (diceTotal == 0)
        {
            StartCoroutine(NoLegalMoveCoroutine());
            return;
        }
        //loop through all of the player's stones
        PlayerStone[] allPlayerStones = GameObject.FindObjectsOfType<PlayerStone>();
        bool hasLegalMove = false;
        foreach (PlayerStone stone in allPlayerStones)
        {
            if (stone.playerId == currentPlayerId)
            {
                if (stone.CanLegallyMoveAhead(diceTotal))
                {
                    //TO DO: highlight stones that can legally be moved
                    hasLegalMove = true;
                }
            }
        }
        //if no legal moves are possible wait a second then move to next player
        if (hasLegalMove == false)
        {
            StartCoroutine(NoLegalMoveCoroutine());
            return;
        }
    }
    IEnumerator NoLegalMoveCoroutine()
    {
        //display message
        NoLegalMovesPopup.SetActive(true);
        //wait a second
        yield return new WaitForSeconds(2f);
        NoLegalMovesPopup.SetActive(false);
        NewTurn();
    }*/
}
