﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitController : MonoBehaviour
{
    //stores all of the stats every units has
    public int maximumActions = 2;
    public int actionsLeft = 2;
    public int maximumLifePoints = 100;
    public int currentLifePoints = 100;
    public int maximumMana = 30;
    public int currentMana = 30;
    //amount of movementpoints gained by a single movement action
    public int movementSpeed = 4;
    public int sightRange = 5;

    //is in sight of the current player
    public bool isInSight = false;
    //is in range for attack of the current player
    public bool isInRange = false;

    // Start is called before the first frame update
    void Start()
    {
        currentLifePoints = maximumLifePoints;
        currentMana = maximumMana;
        actionsLeft = maximumActions;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void BeginTurn()
    {
        //replenish character's actions
        actionsLeft = maximumActions;
        //set the stats the UI displays to those of the active player
        DisplayCharacterValues.instance.SetCurrentPlayerStats();
        this.GetComponent<LineOfSight>().GetLineOfSight();
        //TODO: change stats when effects apply at the beginning of the turn
    }
    public void EndTurn() { 
        //reset the list of all the tiles in sight
        this.GetComponent<LineOfSight>().RemoveTilesInSight();
    }
    //TODO: Set isInSight true if on a tile that is in sight

    /*
    public void UseAction(int actionCost)
    {
        actionsLeft--;
    }*/
}
