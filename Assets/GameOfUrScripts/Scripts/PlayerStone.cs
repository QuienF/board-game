﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStone : MonoBehaviour
{
    /*
    public Tile startingTile;
    public Tile currentTile = null;
    public Tile finalTile = null;
    public Tile[] moveQueue;
    int moveQueueIndex=0;

    bool isAnimating = false;
    bool scoreMe;

    StateManager theStateManager;

    int spacesToMove;
    Vector3 targetPosition;
    Vector3 stoneVelocity = Vector3.zero;

    public int playerId;
    public StoneStorage myStoneStorage;

    public float timeToReachTargetTile;
    public float smoothDistance;
    public float smoothHeight;

    PlayerStone stoneToBop;

    // Start is called before the first frame update
    void Start()
    {
        theStateManager = FindObjectOfType<StateManager>();
        targetPosition = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(isAnimating == false)
        {
            return;
        }

        if (Vector3.Distance(new Vector3(this.transform.position.x , targetPosition.y , this.transform.position.z), targetPosition) < smoothDistance)
        {
            //We have reached the target position; do we still have moves in the queue
            if(moveQueue == null || moveQueueIndex == (moveQueue.Length) &&
                ((this.transform.position.y - smoothDistance) > targetPosition.y))
            {
                //We are out of moves and too high up, the only thing left to do is drop down
                this.transform.position = Vector3.SmoothDamp(this.transform.position, new Vector3(this.transform.position.x, targetPosition.y, this.transform.position.z), ref stoneVelocity, timeToReachTargetTile);
                if (stoneToBop != null)
                {
                    stoneToBop.ReturnToStorage();
                    stoneToBop = null;
                }
            }
            else
            {
                AdvanceMoveQueue();
            }
           
        }
        //Rise stone up before moving sideways 
        else if (this.transform.position.y < (smoothHeight - smoothDistance))
        {
            this.transform.position = Vector3.SmoothDamp(this.transform.position, new Vector3(this.transform.position.x, smoothHeight, this.transform.position.z), ref stoneVelocity, timeToReachTargetTile);
        }
        else
        {
            this.transform.position = Vector3.SmoothDamp(this.transform.position, new Vector3(targetPosition.x, smoothHeight, targetPosition.z), ref stoneVelocity, timeToReachTargetTile);
        }

    }

    void AdvanceMoveQueue()
    {
        //We have reached our last desired position. Do we have another move in our queue?
        if (moveQueue != null && moveQueueIndex < moveQueue.Length)
        {
            Tile nextTile = moveQueue[moveQueueIndex];
            if (nextTile == null)
            {
                //scored
                SetTargetPosition(transform.position + Vector3.right * 10f);

            }
            else
            {
                SetTargetPosition(moveQueue[moveQueueIndex].transform.position);
                moveQueueIndex++;
            }
        }
        else
        {
            Debug.Log("Empty Movequeue, done animating");
            //movement queue is empty so we are done animating
            this.isAnimating = false;
            theStateManager.animationsPlaying--;
        }
    }

    void SetTargetPosition(Vector3 pos)
    {
        targetPosition = pos;
        stoneVelocity = Vector3.zero;
        isAnimating = true;
    }

    private void OnMouseUp()
    {
        //Debug.Log("Clicked");
        spacesToMove = theStateManager.diceTotal;
        //Is this the correct player?
        if (theStateManager.currentPlayerId != playerId)
        {
            //not the correct player
            return;
        }

        if (theStateManager.isDoneRolling == false)
        {
            //cant move yet
            return;
        }
        if (theStateManager.isDoneClicking == true)
        {
            //already done a move
            return;
        }
        

        else
        {
            moveQueue = GetTilesAhead(spacesToMove);
            //Debug.Log("Last movequeue item:" + moveQueue[moveQueue.Length - 1]);
            //finalTile = moveQueue[moveQueue.Length - 1];
            if (finalTile == null || finalTile.isScoringSpace==true)
            {
                //score!
                scoreMe = true;
                //increase score of current player by 1
                theStateManager.currentPlayerScore++;

                //TODO: make stone invisible once scored
                this.GetComponentInChildren<MeshCollider>().enabled = false;
                //this.GetComponentInChildren<MeshRenderer>().enabled = false;
            }


            //check to see if the destination is legal (no other stone on tile)
            else if (finalTile != null)
            {
                if (CanLegallyMoveTo(finalTile) == false)
                {
                    Debug.Log("Tried to move to illegal destination");
                    //not allowed
                    finalTile = currentTile;
                    moveQueue = null;
                    return;
                }
                //if there is an enemy tile kick it off the board
                if (finalTile.playerStone!=null)
                {
                    Debug.Log("Kicking enemy stone off the board");
                    //finalTile.playerStone.ReturnToStorage();
                    stoneToBop = finalTile.playerStone;
                    stoneToBop.currentTile.playerStone = null;
                    stoneToBop.currentTile = null;
                }
            }
            this.transform.SetParent(null);
            // Remove ourselves from our old tile
            if (currentTile != null)
            {
                currentTile.playerStone = null;
            }
            if (finalTile.isScoringSpace == false)   // "Scoring" tiles are always "empty"
            {
                finalTile.playerStone = this;
            }

            // Even before the animation is done, set our current tile to the new tile
            currentTile = finalTile;

            moveQueueIndex = 0;
            currentTile = finalTile;
            theStateManager.isDoneClicking = true;
            this.isAnimating = true;
            theStateManager.animationsPlaying++;
        }
    }
    //return the list of tiles __ spaces ahead of us
    public Tile[] GetTilesAhead(int spacesToMove)
    {
        if (spacesToMove == 0)
        {
            return null;
        }

        // Where should we end up?

        Tile[] listOfTiles = new Tile[spacesToMove];
        finalTile = currentTile;

        for (int i = 0; i < spacesToMove; i++)
        {
            if (finalTile == null && scoreMe == false)
            {
                finalTile = startingTile;
            }
            else
            {
                if (finalTile.nextTile == null || finalTile.nextTile.Length == 0)
                {
                    //we are going of the board and scoring
                    finalTile = null;

                }
                else if (finalTile.nextTile.Length > 1)
                {
                    //Differentiate between Player 1 and Player 2 on branching paths
                    finalTile = finalTile.nextTile[playerId];
                }
                else
                {
                    finalTile = finalTile.nextTile[0];
                }
            }
            listOfTiles[i] = finalTile;
        }

        return listOfTiles;
    }
    //return the final tile we would land on if we moved __ spaces
    Tile GetTileAhead (int spacesToMove)
    {
        Tile[] tiles = GetTilesAhead(spacesToMove);

        if (tiles == null)
        {
            //we arent moving so return the current tile
            return currentTile;
        }
        //return the final tile we want to move to
        return tiles[tiles.Length-1];
    }

    public bool CanLegallyMoveAhead (int spacesToMove)
    {
        Tile theTile = GetTileAhead(spacesToMove);
        return CanLegallyMoveTo(theTile);
    }

    bool CanLegallyMoveTo(Tile destinationTile)
    {
        //is the Tile empty?
        if (destinationTile.playerStone == null)
        {
            return true;
        }
        //is it one of our stones?
        if (destinationTile.playerStone.playerId == this.playerId)
        {
            //We can't land on our own stone
            return false;
        }
        //if it is an enemy stone is it in a save square
        //TO DO: Save Square

        return true;
    }
    public void ReturnToStorage()
    {
        //currentTile.playerStone = null;
        //currentTile = null;

        this.isAnimating = true;
        theStateManager.animationsPlaying++;

        moveQueue = null;

        //save current position
        Vector3 savePosition = this.transform.position;
        myStoneStorage.AddStoneToStorage(this.gameObject);
        theStateManager.animationsPlaying--;
        //set new position to animation target
        SetTargetPosition (this.transform.position);
        //restore saved position
        this.transform.position = savePosition;
    }*/
}
