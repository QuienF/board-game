﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TacticsMove : MonoBehaviour
{
    //is it the unit's turn
    public bool turn = false;

    //tiles the figure can select to move to
    List<Tile> selectableTiles = new List<Tile>();
    public GameObject[] tiles;

    LineOfSight lineOfSight;
    
    //path to walk to get to the target tile
    //path is the tiles to move in reverse order
    Stack<Tile> path = new Stack<Tile>();
    //tile the figure starts on
    Tile currentTile;
    //length of the complete path 
    int completePathLength;
    //flag for when the figure is moving normally meaning it has selected a tile to walk to
    public bool moving = false;
    //TODO: change it to currentMove, add the character speed to it when a movement action is chosen
    //TODO: subtract the amount of times that have been moved so far from the currentMove
    //amount of tiles a moving figure can move
    public int currentMovementPoints = 5;
    public float jumpHeight = 2f;
    //the tile height of the tile the figure is starting from
    int startingTileHeight;
    //used in move function to determine the height of the tile before the one the figure is currently moving to
    int previousTileHeight;

    //the distance at which a targeted tile is considered reached when moving
    public float minDistanceToTarget = 0.05f;


    //speed at which a figure moves (animation) between tiles
    public float animationSpeed = 2f;
    //changes the animation speed when in the air e.g. jumping
    public float airAnimationSpeedModificator;
    //velocity when jumping
    public float jumpVelocity = 4.5f;
    public float hopOffEdgeJumpVelocity = 1.5f;

    //current speed of the moving figure
    Vector3 velocity = new Vector3();
    //direction a figure is heading to
    Vector3 heading = new Vector3();
    
    //get half the height of the tile to use for where the figure sits on top of the tile
    //distance from the center of the tile to the center of the figure
    //IMPORTANT: needs to be reworked if characters are supposed to change their scale e.g. shrink during gameplay
    float halfHeight;

    //flag so selectable tiles are only checked when neccessary
    //had unity crashed before integrating this
    public bool selectableTilesFound = false;

    //needed for Jump Function
    bool fallingDown = false;
    bool jumpingUp = false;
    bool movingToEdge = false;
    Vector3 jumpTarget;
    //flag for when target tileHeight is higher or lower than the current tile
    public bool moveIsAJumpUp;
    public bool moveIsAJumpDown;

    protected void Init()
    {
        //Cache all of the tiles in the scene
        tiles = GameObject.FindGameObjectsWithTag("Tile");

        halfHeight = GetComponent<Collider>().bounds.extents.y;

        TurnManager.AddUnit(this);

        lineOfSight = this.GetComponent<LineOfSight>();
    }
    //Get the tile the figure is currently on
    public void GetCurrentTile()
    {
        currentTile = GetTargetTile(this.gameObject);
        //set flag current of the tile the figure is on
        currentTile.current = true;

        startingTileHeight = currentTile.tileHeight;
        //set it once to the height of the starting tile, so the first iteration of the queue works correctly
        previousTileHeight = startingTileHeight;

    }

    public Tile GetTargetTile(GameObject target)
    {
        RaycastHit hit;
        //if no tile is found return null
        Tile tile = null;

        //found the target tile
        if(Physics.Raycast(target.transform.position, Vector3.down, out hit, 1))
        {
            tile = hit.collider.GetComponent<Tile>();
        }
        return tile;
    }
    //go through adjacency list of the figure
    public void ComputeAdjacencyLists()
    {
        foreach (GameObject tile in tiles)
        {
            Tile t = tile.GetComponent<Tile>();
            t.FindNeighbors();
        }
    }
    public void FindSelectableTiles()
    {
        //go through adjacency list of the figure
        ComputeAdjacencyLists();
        //get tile the figure is sitting on
        GetCurrentTile();

        Queue<Tile> process = new Queue<Tile>();
        //every selectable tile is added to a queue
        process.Enqueue(currentTile);
        //tile has been added to queue so set visited flag
        currentTile.visited = true;
        //currentTile.parent = ?? ; leave as null for now

        //go through entire queue
        while (process.Count>0)
        {
            Tile t = process.Dequeue();
            //add the tile to the selectable tiles and set the selectable flag
            selectableTiles.Add(t);
            t.selectable = true;
            //only keep checking for selectable tiles if the tiles are enough for the figure to reach with its remaining movement points
            if (t.distanceFromStartTile < currentMovementPoints)
            {
                //add every tile adjacent to the currently checked tile, to the queue
                foreach (Tile tile in t.adjencencyList)
                {
                    //only check the tile if it has not already been checked
                    if (tile.visited == false)
                    {
                        tile.parent = t;
                        //tile.parent = null;
                        tile.visited = true;
                        //the currently checked tiles are 1 tile further away
                        tile.distanceFromStartTile = t.distanceFromStartTile + 1;
                        //add the tile to the queue
                        process.Enqueue(tile);
                    }
                }
            }
        }
        selectableTilesFound = true;
    }
    public void MoveToTile(Tile tile)
    {
        //Clear path in case there is something left in it from a previous run
        path.Clear();
        //set the target flag on the tile to move to
        tile.target = true;
        //figure is moving
        moving = true;

        //next tile to move to
        Tile nextTile = tile;
        //has to go through the path in reverse, so once nextTile is null we have reached the starting tile 
        while (nextTile != null)
        {
            path.Push(nextTile);
            nextTile = nextTile.parent;
        }
        completePathLength = path.Count;
    }
    //move a figure from one tile to the next
    public void Move()
    {
        //move as long as there is a tile in the path
        if (path.Count > 0) {
            //look at the next tile in the path
            Tile t = path.Peek();
            //set the character rotation to zero again after every tile 
            this.transform.rotation = Quaternion.identity;
            //check the tileHeight of the tile before the one currently moving to and set the movement flags accordingly
            /*if (t.tileHeight>previousTileHeight)
            {
                moveIsAJumpUp = true;
                moveIsAJumpDown = false;
            }
            else if(t.tileHeight < previousTileHeight)
            {
                moveIsAJumpUp = false;
                moveIsAJumpDown = true;
            }*/
            //removed jumping for now
            moveIsAJumpUp = false;
            moveIsAJumpDown = false;
            
            Vector3 target = t.transform.position;
            //calculate units position on top of the target tile
            //target y position has to be adjusted so the figure is sitting correctly on top of it
            //target.y = halfHeight + t.GetComponent<Collider>().bounds.extents.y;
            target.y += halfHeight;
            target.y += t.GetComponent<Collider>().bounds.extents.y;
            
            //the target tile has been reached when the distance to it is very low
            if (Vector3.Distance(this.transform.position, target) >= minDistanceToTarget)
            {
                if (moveIsAJumpUp == true|| moveIsAJumpDown==true)
                {
                    Jump(target);
                }
                else
                {
                    //calculate and set the direction the figure is heading to if not jumping
                    CalculateHeading(target);
                    SetHorizontalVelocity();

                }
                //movement
                transform.forward = heading;
                transform.position += velocity * Time.deltaTime;
            }
            //reached the next tile
            else
            {
                //normally the starting tile is counted in the path as well
                //this does not count the starting tile so movementpoints are used correctly
                if (path.Count < completePathLength)
                {
                    //remove one movement point after every tile moved
                    currentMovementPoints--;
                    DisplayCharacterValues.instance.SetMovementPointsLeft(currentMovementPoints);
                }

                //put figure on the center of the tile moving to
                this.transform.position = target;
                path.Pop();
                //calculate the line of sight again after every tile
                lineOfSight.RemoveTilesInSight();
                lineOfSight.GetLineOfSight();
                //Reset jump flags after movement
                fallingDown = false;
                jumpingUp = false;
                movingToEdge = false;
                jumpTarget = Vector3.zero;
            }
        }
        //this is the last tile to move to
        else
        {
            //remove the selectable tiles
            RemoveSelectableTiles();
            //find the selectable tiles again but this time from the reached final tile
            //since the target tile has been move the figure is no longer moving
            moving = false;
            selectableTilesFound = false;
        }
    }
    protected void RemoveSelectableTiles()
    {
        if(currentTile != null)
        {
            //we are leaving the current tile so remove the figure from it by setting the current flag
            currentTile.current = false;
            currentTile = null;
        }
        //Reset all the tile's flags etc.
        foreach(Tile tile in selectableTiles)
        {
            tile.Reset();
        }
        selectableTiles.Clear();
    }
    void CalculateHeading(Vector3 target)
    {
        //the directon the figure is traveling
        heading = target - this.transform.position;
        heading.Normalize();
    }
    void SetHorizontalVelocity()
    {
        velocity = heading * animationSpeed;
    }
    
    void Jump (Vector3 target)
    {
        if (fallingDown==true)
        {
            FallDownward(target);
        }
        else if (jumpingUp==true)
        {
            JumpUpward(target);
        }
        else if (movingToEdge == true)
        {
            MoveToEdgeOfTile();
        }
        else
        {
            PrepareJump(target);
        }
    }
    //Set needed values before the jump
    void PrepareJump(Vector3 target)
    {
        Debug.Log("Preparing jump");
        //store the target y position
        float targetY = target.y;
        //set target y temporarily to this y so the heading is correct
        target.y = this.transform.position.y;

        CalculateHeading(target);

        target.y = targetY;

        //figure is higher so will fall down
        if(moveIsAJumpDown==true)
        {
            //will move to edge before falling / jumping down
            fallingDown = false;
            jumpingUp = false;
            movingToEdge = true;
            //halfway point in the right direction which will give the edge of the tile
            jumpTarget = this.transform.position + ((target - this.transform.position) /2.0f);
        }
        //figure is lower so will jump up
        else if (moveIsAJumpUp==true)
        {
            fallingDown = false;
            jumpingUp = true;
            movingToEdge = false;
            //halfway point in the right direction which will give the edge of the tile
            jumpTarget = this.transform.position + ((target - this.transform.position) / 2.0f);

            velocity = heading * animationSpeed * airAnimationSpeedModificator;

            float difference = targetY - transform.position.y;

            velocity.y = jumpVelocity * (0.5f + difference / 2.0f);
        }

    }
    //falling when jumping down
    //falling down when jumping up
    void FallDownward(Vector3 target)
    {
        Debug.Log("falling downward");
        velocity += Physics.gravity * Time.deltaTime;
        //landed on the target so is no longer falling
        if (transform.position.y <= target.y)
        {
            fallingDown = false;
            jumpingUp = false;
            movingToEdge = false;

            //figure is probably slightly below or above the tile so manually set y position of figure
            Vector3 p = this.transform.position;
            p.y = target.y;
            //reset velocity
            velocity = new Vector3();
        }
    }

    void JumpUpward(Vector3 target)
    {
        Debug.Log("jumping upward");
        //affect the velocity by gravity
        velocity += Physics.gravity * Time.deltaTime;
        //have we jumped over the edge of the target?
        //if we do no longer jumpUp but rather fall down
        if(transform.position.y > target.y)
        {
            jumpingUp = false;
            fallingDown = true;
        }
    }

    void MoveToEdgeOfTile()
    {
        Debug.Log("Moving to edge of tile");
        if (Vector3.Distance(this.transform.position, jumpTarget)>= minDistanceToTarget)
        {
            SetHorizontalVelocity();
        }
        //reached edge of tile so is falling down
        else
        {
            Debug.Log("Reached end of tile");
            movingToEdge = false;
            fallingDown = true;

            velocity = velocity * airAnimationSpeedModificator;
            //figure slightly hops when jumping off the edge
            velocity.y = hopOffEdgeJumpVelocity;
        }
    }

    public void BeginTurn()
    {
        currentMovementPoints = 0;
        selectableTilesFound = false;
        turn = true;
        //set the current player to the player who's turn it is
        TurnManager.currentPlayer = this.gameObject;
        //call unit controllers beginning of the turn, resetting the actions
        this.GetComponent<UnitController>().BeginTurn();

        //check line of sight at beginning of turn, so its tilechecking does not interfere with the movement tilechecking
    }

    public void EndTurn()
    {
        currentMovementPoints = 0;
        selectableTilesFound = false;
        turn = false;
        this.GetComponent<UnitController>().EndTurn();
    }
}
