﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    //a state flag of the tile has been changed; set to true so the tiles get colored immediately
    public bool stateHasChanged = true;
    //tile can be walked on
    public bool walkable = true;
    //is true if a figure is on the tile
    public bool current = false;
    //tile that is targeted for moving to it
    public bool target = false;
    //tile can be selected for moving to it
    public bool selectable = false;
    //tile is currently in sight
    public bool inSight = false;
    //tile blocks line of sight
    public bool blocksLineOfSight = false;
    //used for the line of sight check
    public bool checkedForSight = false;

    //TODO: flags for special tiles like water

    //needed for BFS (breadth first search) for checking adjacent tiles
    public bool visited = false;
    public Tile parent = null;
    //check how many tiles away a checked tile is from the starting tile (movement)
    public int distanceFromStartTile = 0;
    //check how many tiles away a checked tile is from the starting tile (sight)
    public int sightDistanceFromStartTile = 0;

    //Height of the tile
    public int tileHeight = 0;

    public List<Tile> adjencencyList = new List<Tile>();
    public List<Tile> sightList = new List<Tile>();
    
    Material defaultTileMaterial;
    Color defaultTileColor;

    // Start is called before the first frame update
    void Start()
    {
        defaultTileMaterial = this.GetComponentInChildren<Renderer>().material;
        defaultTileColor = this.GetComponentInChildren<Renderer>().material.color;
        //set the collider to match the visual
        this.GetComponent<Collider>().transform.position = this.GetComponentInChildren<Transform>().transform.position;
        
        //highlight tiles according to their state
        //TileHighlighting();
    }

    // Update is called once per frame
    void Update()
    {
        //highlight tiles according to their state if any state has changed
        if (stateHasChanged == true)
        {
            TileHighlighting();
            stateHasChanged = false;
        }
        
    }

    public void ResetTileMaterial()
    {
        //resets the tile material and material color to its default
        this.GetComponentInChildren<Renderer>().material = defaultTileMaterial;
        this.GetComponentInChildren<Renderer>().material.color = defaultTileColor;
    }

    //TODO: currently checks and sets color every frame, which makes it very costly processingwise
    void TileHighlighting()
    {
        //tile can be selected AND is in sight of the unit
        if (selectable == true && inSight == true)
        {
            this.GetComponentInChildren<Renderer>().material.color = Color.red + Color.yellow;
        }
        //tile with a figure on
        else if (current == true)
        {
            this.GetComponentInChildren<Renderer>().material.color = Color.magenta;
        }
        //tile is in sight of current unit
        else if (inSight == true)
        {
            this.GetComponentInChildren<Renderer>().material.color = Color.yellow;
        }
        //selectable tile
        else if (selectable == true)
        {
            this.GetComponentInChildren<Renderer>().material.color = Color.red;
        }
        //tile has been targeted for movement
        else if (target == true)
        {
            this.GetComponentInChildren<Renderer>().material.color = Color.green;
        }
        else
        {
            ResetTileMaterial();
        }
    }
    //find adjacent tiles
    public void FindNeighbors()
    {
        Reset();
        //Check tile in every direction
        CheckTile(Vector3.forward, tileHeight);
        CheckTile(Vector3.back, tileHeight);
        CheckTile(Vector3.right, tileHeight);
        CheckTile(Vector3.left, tileHeight);
    }

    //find diagonally adjacent tiles
    public void FindDiagonalTiles()
    {
        //check tile in every diagonal direction
        //upper right
        CheckTile(Quaternion.Euler(0, 45, 0) * Vector3.forward, tileHeight);
        //lower right
        CheckTile(Quaternion.Euler(0, 135, 0) * Vector3.forward, tileHeight);
        //lower left
        CheckTile(Quaternion.Euler(0, 225, 0) * Vector3.forward, tileHeight);
        //upper left
        CheckTile(Quaternion.Euler(0, 315, 0) * Vector3.forward, tileHeight);
    }

    public void FindSightTiles()
    {
        FindNeighbors();
        FindDiagonalTiles();
    }

    //check tiles where sight is blocked from another tile
    public void CheckTilesForBlockedSight(Vector3 direction)
    {
        bool keepChecking = true;
        //check tiles in the direction until no tile is found
        //while (keepChecking == true)
        //{
            //the half of a tile
            Vector3 halfExtents = new Vector3(0.25f, ((1 + tileHeight) / 2.0f), 0.25f);
            //keep going in the same direction
            Vector3 checkPosition = transform.position + direction;
            Collider[] colliders = Physics.OverlapBox(checkPosition, halfExtents);
            foreach (Collider item in colliders)
            {
                Tile tile = item.GetComponent<Tile>();
                //if it is null it is not a tile so ignore it
                if (tile != null)
                {
                    tile.inSight = false;
                    tile.checkedForSight = true;
                }
                else
                {
                    Debug.Log("keep checking is false");
                    keepChecking = false;
                }
            }
        //}
    }

    public void CheckTile(Vector3 direction, float tileHeight)
    {
        //the half of a tile
        Vector3 halfExtents = new Vector3(0.25f,((1+tileHeight) / 2.0f),0.25f);
        Collider[] colliders = Physics.OverlapBox(transform.position + direction, halfExtents);
        foreach (Collider item in colliders)
        {
            Tile tile = item.GetComponent<Tile>();
            //if it is null it is not a tile so ignore it
            if(tile != null)
            {
                //only check if the tile is actually walkable
                //check for a figure on the currently checked tile
                //TODO: add additional checks for determining whether a tile is walkable or not
                tile.walkable = CheckTileForFigure(tile.transform.position);
                if (tile.walkable == true)
                {
                    adjencencyList.Add(tile);
                }
                //only add the tile to line of sight list if it does not block sight
                //if (tile.blocksLineOfSight == false)
                //{
                    sightList.Add(tile);
                //}
                
            }
        }
    }
    //Check if a figure is on this tile
    public bool CheckTileForFigure(Vector3 tilePosition)
    {
        RaycastHit hit;
        //if a raycast from the tile hits a collider it means there is something on the tile
        if ((Physics.Raycast(tilePosition, Vector3.up, out hit, 1))==false)
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }

    //Reset everything to default value
    public void Reset()
    {
        adjencencyList.Clear();
        current = false;
        target = false;
        selectable = false;
        visited = false;
        distanceFromStartTile = 0;      
        stateHasChanged = true;
        parent = null;
    }
    public void TileSightReset()
    {
        adjencencyList.Clear();
        checkedForSight = false;
        inSight = false;
        stateHasChanged = true;
        sightDistanceFromStartTile = 0;
        parent = null;
    }
}