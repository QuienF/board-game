﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineOfSight : UnitController
{
    TacticsMove tacticsMove;
    Tile currentTile;

    bool lineOfSightFound = true;
    List<Tile> tilesInSight = new List<Tile>();

    //the direction the sight blocking tile is in relation to the player
    bool tileIsToTheLeft = false;
    bool tileIsToTheRight = false;
    bool tileIsInFront = false;
    bool tileIsBehind = false;
    bool tileIsInSameRow = false;
    bool tileIsInSameColum = false;

    // Start is called before the first frame update
    void Start()
    {
        tacticsMove = this.GetComponent<TacticsMove>();
        if (tacticsMove.turn == true && tilesInSight == null)
        {
            GetLineOfSight();
        }
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (tacticsMove.turn == true&&tilesInSight==null)
        {
            GetLineOfSight();
        }*/        
    }

    public void GetLineOfSight()
    {
        Debug.Log("Checking line of sight");
        //get all of the tiles in every direction including diagonals
        ComputeDiagonalAdjacencyLists();
        GetCurrentTile();

        Queue<Tile> process = new Queue<Tile>();
        //every selectable tile is added to a queue
        process.Enqueue(currentTile);
        //tile has been added to queue so set visited flag
        currentTile.checkedForSight = true;
        //currentTile.parent = ?? ; leave as null for now

        //go through entire queue
        while (process.Count > 0)
        {
            Tile t = process.Dequeue();
            //add the tile to the tiles in sight and set the insight flag
            tilesInSight.Add(t);
            t.inSight = true;
            //if the tile blocks sight check every tile whos sight is blocked and set it accordingly depending on the position of the player in relation to the blocking tile
            if (t.blocksLineOfSight == true)
            {
                SightBlockedTiles(t);
            }
                //only keep checking for selectable tiles if the tiles are in sight range of the unit
            if (t.sightDistanceFromStartTile < this.GetComponent<UnitController>().sightRange)
            {
                Debug.Log("checking with sightrange"+ this.GetComponent<UnitController>().sightRange);
                //add every tile adjacent to the currently checked tile, to the queue if it does not block the line of sight
                foreach (Tile tile in t.sightList)
                {
                        //only check the tile if it has not already been checked
                        if (tile.checkedForSight == false)
                        {
                            tile.parent = t;
                            tile.checkedForSight = true;
                            //the currently checked tiles are 1 tile further away
                            tile.sightDistanceFromStartTile = t.sightDistanceFromStartTile + 1;
                            //add the tile to the queue
                            process.Enqueue(tile);
                        }
                }
            }
        }
        lineOfSightFound = true;
    }

    void SightBlockedTiles(Tile t)
    {
        GetPositionDirectionOfBlockingTile(t);

        Debug.Log("tile is to the left:" + tileIsToTheLeft);
        Debug.Log("tile is in front:" + tileIsInFront);
        //find out in which direction the wall is
        Tile tile = t;
        if (tileIsInSameRow == true&&tileIsToTheLeft==true)
        {
            Debug.Log("tile is directly to the left");
            tile.CheckTilesForBlockedSight(Vector3.left);
        }
        //tile is to the upper left
        else if (tileIsToTheLeft == true && tileIsInFront == true)
        {
            //Check tiles to the left, up and leftup diagonally
            //tile.CheckTilesForBlockedSight(Vector3.forward);
            tile.CheckTilesForBlockedSight(Vector3.left);
            //upper left
            tile.CheckTilesForBlockedSight(Quaternion.Euler(0, 315, 0) * Vector3.forward);
        }
        //tile is behind to the right
        else if (tileIsToTheRight == true && tileIsBehind == true)
        {
            //Check tiles to the right, down and downright diagonally
            tile.CheckTilesForBlockedSight(Vector3.right);
            //tile.CheckTilesForBlockedSight(Vector3.back);
            //lower right
            tile.CheckTilesForBlockedSight(Quaternion.Euler(0, 135, 0) * Vector3.forward);
        }
        //tile is to the upper left
        else if (tileIsToTheLeft == false && tileIsInFront == true)
        {
            //Check tiles to the right, up and rightup diagonally
            //tile.CheckTilesForBlockedSight(Vector3.forward);
            tile.CheckTilesForBlockedSight(Vector3.right);
            //upper right
            tile.CheckTilesForBlockedSight(Quaternion.Euler(0, 45, 0) * Vector3.forward);

        }
        //tile is to the upper left
        else if (tileIsToTheLeft == true && tileIsInFront == false)
        {
            //Check tiles to the left, back and leftback diagonally
            tile.CheckTilesForBlockedSight(Vector3.left);
            //tile.CheckTilesForBlockedSight(Vector3.back);
            //lower left
            tile.CheckTilesForBlockedSight(Quaternion.Euler(0, 225, 0) * Vector3.forward);
        }
        //tile is to the upper left
        else
        {
            //probably tile the unit is standing on
        }
    }

    void GetPositionDirectionOfBlockingTile(Tile t)
    {
        Vector3 unitPosition = this.gameObject.transform.position;
        Vector3 blockingTilePosition = t.transform.transform.position;
        

        //is the blocking tile to the right or to the left of the player
        if (blockingTilePosition.x > unitPosition.x)
        {
            tileIsToTheLeft = false;
            tileIsToTheRight = true;
            tileIsInSameColum = false;
        }
        else if (blockingTilePosition.x < unitPosition.x)
        {
            tileIsToTheLeft = true;
            tileIsToTheRight = false;
            tileIsInSameColum = false;
        }
        else
        {
            tileIsToTheLeft = false;
            tileIsToTheRight = false;
            tileIsInSameColum = true;
        }
        //is the blocking tile in front of behind the player or in the same row
        if (blockingTilePosition.z < unitPosition.z)
        {
            tileIsInFront = false;
            tileIsBehind = true;
            tileIsInSameRow = false;
        }
        else if (blockingTilePosition.z > unitPosition.z)
        {
            tileIsInFront = true;
            tileIsBehind = false;
            tileIsInSameRow = false;
        }
        else
        {
            tileIsInFront = false;
            tileIsBehind = false;
            tileIsInSameRow = true;
        }
    }

    public void ComputeDiagonalAdjacencyLists()
    {
        foreach (GameObject tile in tacticsMove.tiles)
        {
            Tile t = tile.GetComponent<Tile>();
            //only check the tile's adjacent tiles if it does not block line of sight
            t.FindSightTiles();
        }
    }
    //get the tile the unit is standing on
    void GetCurrentTile()
    {
        currentTile = tacticsMove.GetTargetTile(this.gameObject);
    }

    public void RemoveTilesInSight()
    {
        Debug.Log("removing tiles in sight");
        if (currentTile != null)
        {
            //we are leaving the current tile so remove the figure from it by setting the current flag
            currentTile.current = false;
            currentTile = null;
        }
        //Reset all the tile's flags etc.
        foreach (Tile tile in tilesInSight)
        {
            tile.TileSightReset();
        }
        tilesInSight.Clear();
    }
}
